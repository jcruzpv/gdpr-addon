<?php

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link       https://www.primeview.com
 * @since      1.0.0
 *
 * @package    Gdpr_Addon
 * @subpackage Gdpr_Addon/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    Gdpr_Addon
 * @subpackage Gdpr_Addon/includes
 * @author     Primeview LLC <john@primeview.com>
 */
class Gdpr_Addon {

	/**
	 * The loader that's responsible for maintaining and registering all hooks that power
	 * the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      Gdpr_Addon_Loader    $loader    Maintains and registers all hooks for the plugin.
	 */
	protected $loader;

	/**
	 * The unique identifier of this plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $plugin_name    The string used to uniquely identify this plugin.
	 */
	protected $plugin_name;

	/**
	 * The current version of the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $version    The current version of the plugin.
	 */
	protected $version;

	/**
	 * Define the core functionality of the plugin.
	 *
	 * Set the plugin name and the plugin version that can be used throughout the plugin.
	 * Load the dependencies, define the locale, and set the hooks for the admin area and
	 * the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function __construct() {
		if ( defined( 'PLUGIN_NAME_VERSION' ) ) {
			$this->version = PLUGIN_NAME_VERSION;
		} else {
			$this->version = '1.0.0';
		}
		$this->plugin_name = 'gdpr-addon';

		$this->load_dependencies();
		$this->set_locale();
		$this->define_admin_hooks();
		$this->define_public_hooks();

	}

	/**
	 * Load the required dependencies for this plugin.
	 *
	 * Include the following files that make up the plugin:
	 *
	 * - Gdpr_Addon_Loader. Orchestrates the hooks of the plugin.
	 * - Gdpr_Addon_i18n. Defines internationalization functionality.
	 * - Gdpr_Addon_Admin. Defines all hooks for the admin area.
	 * - Gdpr_Addon_Public. Defines all hooks for the public side of the site.
	 *
	 * Create an instance of the loader which will be used to register the hooks
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function load_dependencies() {

		/**
		 * The class responsible for orchestrating the actions and filters of the
		 * core plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-gdpr-addon-loader.php';

		/**
		 * The class responsible for defining internationalization functionality
		 * of the plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-gdpr-addon-i18n.php';

		/**
		 * The class responsible for defining all actions that occur in the admin area.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-gdpr-addon-admin.php';

		/**
		 * The class responsible for defining all actions that occur in the public-facing
		 * side of the site.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/class-gdpr-addon-public.php';

		$this->loader = new Gdpr_Addon_Loader();

	}

	/**
	 * Define the locale for this plugin for internationalization.
	 *
	 * Uses the Gdpr_Addon_i18n class in order to set the domain and to register the hook
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function set_locale() {

		$plugin_i18n = new Gdpr_Addon_i18n();

		$this->loader->add_action( 'plugins_loaded', $plugin_i18n, 'load_plugin_textdomain' );

	}

	/**
	 * Register all of the hooks related to the admin area functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_admin_hooks() {

		$plugin_admin = new Gdpr_Addon_Admin( $this->get_plugin_name(), $this->get_version() );

		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_styles' );
		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_scripts' );

	}

	/**
	 * Register all of the hooks related to the public-facing functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_public_hooks() {

		$plugin_public = new Gdpr_Addon_Public( $this->get_plugin_name(), $this->get_version() );

		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_styles' );
		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_scripts' );

	}

	/**
	 * Run the loader to execute all of the hooks with WordPress.
	 *
	 * @since    1.0.0
	 */
	public function run() {
		$this->loader->run();
		
		/*Hooks*/
		add_filter( 'page_template', array(&$this, 'user_privacy_security_template') );
		add_action( 'wp_head',array(&$this, 'ask_for_consent')); 
		add_shortcode('gdpr-checkbox', array(&$this, 'gdpr_checkbox')); //[gdpr-checkbox]
		add_filter( 'wpcf7_form_elements', array(&$this, 'mycustom_wpcf7_form_elements') );
		
		add_action( 'admin_post_nopriv_gdpr_form', array(&$this, 'gdpr_admin_post_action') );
		add_action( 'admin_post_gdpr_form', array(&$this, 'gdpr_admin_post_action') );
		
		add_action('admin_menu',array(&$this, 'gdpr_main_pages'));
		
		add_action( 'admin_init',array(&$this, 'gdpr_save_settings_post')); 
		
		if(!is_admin()){
			//wp_enqueue_style( 'bootstrap-gdpr', '//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css');
		}
	} 

	/**
	 * The name of the plugin used to uniquely identify it within the context of
	 * WordPress and to define internationalization functionality.
	 *
	 * @since     1.0.0
	 * @return    string    The name of the plugin.
	 */
	public function get_plugin_name() {
		return $this->plugin_name;
	}

	/**
	 * The reference to the class that orchestrates the hooks with the plugin.
	 *
	 * @since     1.0.0
	 * @return    Gdpr_Addon_Loader    Orchestrates the hooks of the plugin.
	 */
	public function get_loader() {
		return $this->loader;
	}

	/**
	 * Retrieve the version number of the plugin.
	 *
	 * @since     1.0.0
	 * @return    string    The version number of the plugin.
	 */
	public function get_version() {
		return $this->version;
	}
	
	/**
	 * Plugin Customization
	 */
	function gdpr_main_pages(){
		add_menu_page( 
			'GDPR', 							 // Page Title
			'PV GDPR',           				 // Navbar Title
			'manage_options',      					 // Permission 
			'pv-gdpr',      					 // Page ID
			array(&$this, 'gdpr_page'),           			 	  	 // Function call
			'dashicons-clipboard',   					 // Favicon
			2                				 	// Order
		);	
		add_submenu_page( 
			'pv-gdpr',      			 		 // Parent Page ID
			'Settings',     		 		 // Page Title
			'Settings', 						 // Navbar Title
			'manage_options', 						 // Permission 	
			'gdpr-settings', 							 // Submenu Page ID
			array(&$this, 'gdpr_settings')	 		 // Function  call	 
		); 		 
	}
	function gdpr_generate_tabs( $current = 'rectification' ){
	   $tabs = array(
			'rectification'   => __( 'Rectification Requests', 'plugin-textdomain' ), 
			'complaints'  => __( 'Complaints', 'plugin-textdomain' ),
			'delete'  => __( 'Deletion Requests', 'plugin-textdomain' ),
			'download'  => __( 'Data Download Requests', 'plugin-textdomain' ),
		);
		$html = '<h2 class="nav-tab-wrapper">';
		foreach( $tabs as $tab => $name ){
			$class = ( $tab == $current ) ? 'nav-tab-active' : '';
			$html .= '
				<a class="nav-tab ' . $class . '" href="?page=pv-gdpr&tab=' . $tab . '">' . $name . '</a>

			';
		}
		$html .= '</h2>';
		echo $html;		
	}
	function gdpr_page(){
		if (class_exists('CF7DBPlugin')) {
			if( ! empty( $_GET['tab'] ) ) {
				$active =  $_GET['tab'];
				if($active == 'complaints'){
					$this->gdpr_generate_tabs('complaints');
					echo '
							<div class="gdpr-tab complaints">
								'.do_shortcode('[cfdb-datatable form="gdpr-complaint-requests"]').'
							</div>
						';
				}else if($active == 'delete'){
					$this->gdpr_generate_tabs('delete');
					echo '
							<div class="gdpr-tab delete">
								'.do_shortcode('[cfdb-datatable form="gdpr-deletion-requests"]').'
							</div>
						';
				}else if($active == 'download'){
					$this->gdpr_generate_tabs('download');
					echo '
							<div class="gdpr-tab download">
								'.do_shortcode('[cfdb-datatable form="gdpr-download-requests"]').'
							</div>
						';
				}
			}else{
					$this->gdpr_generate_tabs('rectification');
					echo '
							<div class="gdpr-tab rectification">
								'.do_shortcode('[cfdb-datatable form="gdpr-rectification-requests"]').'
							</div>
						';
				}
		}else{
			echo "You must activate CFDB Plugin first.";
		}
	}
	public function gdpr_settings(){
		echo '<h2>PrimeView GDPR Addons Settings Page</h2>';
		echo '<form method="post" action="options.php">';
				settings_fields( 'pvgdpr-option-group' );
				do_settings_sections( 'pvgdpr-option-group' );
		echo '<h4>Email Notification</h4>';
		echo '<input  placeholder="Email Address" type="text" name="pvgdpr_admin_email" value="'. esc_attr( get_option('pvgdpr_admin_email') ).'" />';
		echo '<h4>Policy Pages</h4><hr/>';
		echo '<input placeholder="/privacy-policy" type="text" name="pvgdpr_privacy_policy" value="'. esc_attr( get_option('pvgdpr_privacy_policy') ).'" /><br/>';		
		echo '<h4>Cookie Policy</h4>';
		echo '<input placeholder="/cookie-policy" type="text" name="pvgdpr_cookie_policy" value="'. esc_attr( get_option('pvgdpr_cookie_policy') ).'" /><br/>';				
		echo '<h4>Terms and Conditions</h4>';
		echo '<input placeholder="/terms-and-conditions" type="text" name="pvgdpr_terms_conditions" value="'. esc_attr( get_option('pvgdpr_terms_conditions') ).'" />';						
				submit_button();
		echo '</form>';
	}
	public function gdpr_save_settings_post(){
		register_setting( 'pvgdpr-option-group', 'pvgdpr_admin_email' );
		register_setting( 'pvgdpr-option-group', 'pvgdpr_privacy_policy' );
		register_setting( 'pvgdpr-option-group', 'pvgdpr_cookie_policy' );
		register_setting( 'pvgdpr-option-group', 'pvgdpr_terms_conditions' );
	}
	public function mycustom_wpcf7_form_elements( $form ) {
		$form = do_shortcode( $form );

		return $form;
	}	
	public function ask_for_consent(){
		$privacy_policy = !empty(get_option('pvgdpr_privacy_policy')) ?  get_option('pvgdpr_privacy_policy') : '/privacy-policy';
		$cookie_policy = !empty(get_option('pvgdpr_cookie_policy')) ?  get_option('pvgdpr_cookie_policy') : '/cookie-policy';
		$terms_conditions = !empty(get_option('pvgdpr_terms_conditions')) ?  get_option('pvgdpr_terms_conditions') : '/terms-and-conditions';		
		$consent = '
			<link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.3/cookieconsent.min.css" />
			<script src="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.3/cookieconsent.min.js"></script>
			<script type="text/javascript">
			 
			function disableCookie(){ 
				if(!document.__defineGetter__) {
					Object.defineProperty(document, "cookie", {
						get: function(){return ""},
						set: function(){return true},
					});
				} else {
					document.__defineGetter__("cookie", function() { return "";} );
					document.__defineSetter__("cookie", function() {} );
				}				
				console.log("Cookie Disabled");
			}
			
			window.addEventListener("load", function(){
			window.cookieconsent.initialise({
			onInitialise: function (status) {
			  var type = this.options.type;
			  var didConsent = this.hasConsented();
			  if (type == "opt-in" && didConsent) {
				// enable cookies
				console.log("Enabled Cookie");
			  }
			  if (type == "opt-out" && !didConsent) {
				// disable cookies
				disableCookie();
			  }
			},
			 
			onStatusChange: function(status, chosenBefore) {
			  var type = this.options.type;
			  var didConsent = this.hasConsented();
			  if (type == "opt-in" && didConsent) {
				// enable cookies
				console.log("Enabled Cookie");
			  }
			  if (type == "opt-out" && !didConsent) {
				// disable cookies
				disableCookie();
			  }
			},
			 
			onRevokeChoice: function() {
			  var type = this.options.type;
			  if (type == "opt-in") {
				// disable cookies
				disableCookie();
			  }
			  if (type == "opt-out") {
				// enable cookies
				console.log("Enabled Cookie");
			  }
			},				
			  "palette": {
				"popup": {
				  "background": "#000"
				},
				"button": {
				  "background": "#f1d600"
				} 
			  },
			  "showLink": false,
			  "theme": "classic",
			 //"position": "top", 
			  "type": "opt-out", 
			  "content": {
				"message": "<h4>'.get_bloginfo('name').'</h4> Our Website uses cookies to improve your experience. Please visit our <a href='.$privacy_policy.'>Privacy Policy</a> page for more information about cookies and how we use them.",
				"dismiss": "Accept",
				"deny": "Decline" 
			  }
			})});
			</script>
		';
		echo $consent;
	}	
	public function gdpr_checkbox(){
		$privacy_policy = !empty(get_option('pvgdpr_privacy_policy')) ?  get_option('pvgdpr_privacy_policy') : '/privacy-policy';
		$cookie_policy = !empty(get_option('pvgdpr_cookie_policy')) ?  get_option('pvgdpr_cookie_policy') : '/cookie-policy';
		$terms_conditions = !empty(get_option('pvgdpr_terms_conditions')) ?  get_option('pvgdpr_terms_conditions') : '/terms-and-conditions';
		$consent =  '
			<div class="gdpr-checkbox-cf7">
				<input type="checkbox" class="gdpr-consent" /value="yes"> 
				I consent this site to collect my Information.
				<p>This form collects your information so that we can correspond with you.
				Please check our <a href="'.$privacy_policy.'"><u>Privacy Policy</u></a> and <a href="'.$terms_conditions.'">Terms and Conditions</a>.</p>
			</div>
			<script type="text/javascript">
				$x = jQuery.noConflict();
				$x(function(){  
					console.log("GDPR for cf...............100%");
					var consent = $x("input.gdpr-consent"); 
					var submit = $x(".wpcf7-submit:not(.disregard)");
					var form = submit.closest("form");
					form.attr("onsubmit","false");
					var flag = false;
					submit.click(function(e){ 
						console.log(e);
						if(!consent.prop("checked")){
							console.log(false);
							e.preventDefault();
							alert("You must give consent to this site to collect your information.");
						}else{
							console.log(true);
							form.attr("onsubmit","true");
						}
					});
				});
			</script>
		';
		return $consent;
	}
	public function user_privacy_security_template($page_template){
		if ( is_page( 'user-data-security-and-privacy-help-center' ) ) {
			$page_template = dirname( __FILE__ ) . '/templates/gdpr-template.php';
		}
		return $page_template;		
	}
	
	/*Form Action*/
	function gdpr_admin_post_action() {
		$domain = 'noreply@'.preg_replace('/www\./i', '', $_SERVER['SERVER_NAME']);
		
		$type = sanitize_text_field( $_POST['type'] );
		$email = sanitize_text_field( $_POST['email'] );
	
		if(isset($_POST['message'])){
			$message = sanitize_text_field( $_POST['message'] );
		}
		
		$headers = 'From:'.$domain.''."\r\n";
		
		$to = 'john@primeview.com,lorelaine@primeview.com';

		if(!empty(esc_attr( get_option('pvgdpr_admin_email') ))){
			$to = esc_attr( get_option('pvgdpr_admin_email') );
		}
		
		
		if($type == 'rectify'){
			$subject = 'Rectification Notification';
			$message = '
				Email : '.$email.'
				Message : '.$message.'
			';
			$reply = 'Your request to correct your data has been received. Please wait for further updates. Thank you.';
		}
		else if($type == 'complaint'){
			$subject = 'Complaint Notification';
			$message = '
				Email : '.$email.'
				Message : '.$message.'
			';
			$reply = 'Your complaint has been received. Please wait for further updates. Thank you.';
		}	
		else if($type == 'delete'){
			$subject = 'User Data Deletion Notification';
			$message = 'User Data Deletion Request for : '.$email.'';
			$reply = 'Your request to delete your data has been received. Please wait for further updates. Thank you.';
		}		
		else if($type == 'download'){
			$subject = 'User Data Information Download Request Notification';
			$message = 'User Data Information Download Request for : '.$email.'';
			$reply = 'Your request to download your data has been received. Please wait for further updates. Thank you.';
		}	
		
		$this->insertToCFDB($email,$message,$type);
		$response = wp_mail($to,$subject,$message,$headers);
		$autosend =  wp_mail($email,$subject,$reply,$headers);
		
		if($response){
			wp_redirect( "/thank-you", 301 );
			exit();
		}	
	}
	/*Insert To CFDatabase*/
	public function insertToCFDB($email,$message,$type){
		if($type == 'rectify'){
			$data = (object) array(
				'title' => 'gdpr-recrification-requests',
				'posted_data' => array(
					'email' => $email,
					'message' => $message
				),
				'uploaded_files' => null
			);	
		}else if($type == 'complaint'){
			$data = (object) array(
				'title' => 'gdpr-complaint-requests',
				'posted_data' => array(
					'email' => $email,
					'message' => $message
				),
				'uploaded_files' => null
			);	
		}else if($type == 'delete'){
			$data = (object) array(
				'title' => 'gdpr-deletion-requests',
				'posted_data' => array(
					'email' => $email,
					'message' => $message
				),
				'uploaded_files' => null
			);	
		}else if($type == 'download'){
			$data = (object) array(
				'title' => 'gdpr-download-requests',
				'posted_data' => array(
					'email' => $email,
					'message' => $message
				),
				'uploaded_files' => null
			);	
		}

		if (class_exists('CF7DBPlugin')) {
			do_action_ref_array( 'cfdb_submit', array( &$data ) );
			return true;
		}		
	}
} 
