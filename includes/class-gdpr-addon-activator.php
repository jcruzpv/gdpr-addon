<?php

/**
 * Fired during plugin activation
 *
 * @link       https://www.primeview.com
 * @since      1.0.0
 *
 * @package    Gdpr_Addon
 * @subpackage Gdpr_Addon/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Gdpr_Addon
 * @subpackage Gdpr_Addon/includes
 * @author     Primeview LLC <john@primeview.com>
 */
class Gdpr_Addon_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
