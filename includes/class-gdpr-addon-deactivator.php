<?php

/**
 * Fired during plugin deactivation
 *
 * @link       https://www.primeview.com
 * @since      1.0.0
 *
 * @package    Gdpr_Addon
 * @subpackage Gdpr_Addon/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Gdpr_Addon
 * @subpackage Gdpr_Addon/includes
 * @author     Primeview LLC <john@primeview.com>
 */
class Gdpr_Addon_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
